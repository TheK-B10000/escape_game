// Fill out your copyright notice in the Description page of Project Settings.

#include "PositionReport_02.h"


// Sets default values for this component's properties
UPositionReport_02::UPositionReport_02()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UPositionReport_02::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogTemp, Warning, TEXT("Position report_02 reporting for duty on column!"));
	
}


// Called every frame
void UPositionReport_02::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

